
function transformAndDiff(){
	transform();
	prettifyAndDiff();
}

function transform()
{
	var inputJson = document.getElementById("input").value;
	
	try{
		var json = parseJson(inputJson);

		//Main
		updateRcuConfig(json);

		showResultInTextArea(json);
	}catch(err){
		console.error(err);
		alert(err);
	}

	updateTimestamp();
}
function prettifyAndDiff()
{
	var inputJson = document.getElementById("input").value;
	var json = parseJson(inputJson);
	document.getElementById("input").innerHTML = prettyPrintJson(json);

	diffUsingDifflib(
		document.getElementById("input").value,
		document.getElementById("output").value
	);
}

function parseJson(inputJson){
	if(!testJSON(inputJson)){
		throw 'Invalid JSON format'; 
	}

	return JSON.parse(inputJson);
}

function updateRcuConfig(json){
	json.rcu.forEach(rcu=>{

		if(isAddButler() === true){
			addButlerSwitchIfEmpty(rcu);
		}

		if(isAddLaundry() === true){
			addLaundrySwitchIfEmpty(rcu);
		}

		if(isChangeTemperatureReadValTo19AE() === true){
			changeRcuTemperatureReadValueTo19AE(rcu);
		}

		if(isRemoveCurtainSwitch() === true){
			removeCurtainSwitch(rcu);
		}

		if(isAddAlarmConfig() === true){
			addAlarmConfig(rcu);
		}

		var hvacControl = rcu.command.controls.filter(control=>{
			return control.type === "Hvac"
		});
		
		if(isChangeAcAllowReadTrue() === true){
			changeAcReadToTrue(hvacControl);
		}

		if(isChangeAcCtrlChannel00() === true){
			changeAcTemperaureChannelTo00(hvacControl);
		}

	});


	if(isAdd6Delay() === true){
		add6Delay(json.mvi);
	}

}


function changeAcReadToTrue(hvacControl){
	hvacControl.forEach(h=>{
		if(h.controlType==="PanelControl"){
			var tempUnitTypes = h.additionalTypes.filter(at=>{
				return at.parameter === "TemperatureUnitType"
			});

			if(isEmptyArr(tempUnitTypes)){
				h.additionalTypes.unshift(
					{
						"allowRead": "true",
						"parameter": "TemperatureUnitType"
					}
				);
			}else{
				tempUnitTypes.forEach(at=>{
					if(at.parameter === "TemperatureUnitType"){
						at.allowRead = "true"
					}
				});
			}
		}
	});
}

function changeAcTemperaureChannelTo00(hvacControl){
	hvacControl
	.filter(h=>h.controlType==="Temperature")
	.forEach(h=> {
		h.channel = "00";
	});
}

function changeRcuTemperatureReadValueTo19AE(rcu){
	rcu.command.controlTypes
			.filter(cType=>{
				return cType.controlType === "Temperature"
			}).forEach(ctrlType=>{
				ctrlType.commandTypes.forEach(cmdType=>{
					if(cmdType.parameter === "Read"){
						cmdType.value = "19 AE";
					}
				})
			});
}

function add6Delay(mvi){
	addValueIfNotExist(mvi, "BUTLER_DELAY", 120000);
	addValueIfNotExist(mvi, "LAUNDRY_DELAY", 120000);
	addValueIfNotExist(mvi, "MAKE_UP_ROOM_DELAY", 120000);
	addValueIfNotExist(mvi, "BUTLER_DISMISS_DELAY", 600000);
	addValueIfNotExist(mvi, "LAUNDRY_DISMISS_DELAY", 600000);
	addValueIfNotExist(mvi, "MAKE_UP_ROOM_DISMISS_DELAY", 60000);
}

function addButlerSwitchIfEmpty(rcu){
	var butlerControl = rcu.command.controls
			.filter(control=>{
				return control.id === "Common_Butler"
			});

	if(isEmptyArr(butlerControl)){
		rcu.command.controls.unshift(
			{
				"id": "Common_Butler",
				"type": "Courtesy",
				"channel": "0C",
				"deviceId": "0A",
				"subnetId": "01",
				"allowRead": "true",
				"controlType": "UvSwitch"
			}
		);
	}
}

function addLaundrySwitchIfEmpty(rcu){
	var laundryControl = rcu.command.controls
			.filter(control=>{
				return control.id === "Common_Laundry"
			});

	if(isEmptyArr(laundryControl)){
		rcu.command.controls.unshift(
			{
				"id": "Common_Laundry",
				"type": "Courtesy",
				"channel": "18",
				"deviceId": "0A",
				"subnetId": "01",
				"allowRead": "true",
				"controlType": "UvSwitch"
			}
		);
	}
}

function addAlarmConfig(rcu){
	if(isEmptyArr(rcu["alarmConfig"])){

		var bedroomControl = rcu.command.controls.filter(control=>{
				return control.id.startsWith("BedRoom");
			});

		if(isEmptyArr(bedroomControl)){
			alert('[AddAlarm] No Id starts with "Bedroom" is found. No change for adding alarms...');
			return;
		}
		let zoneId = bedroomControl[0].id.split("_",1);

		let alarmPanelId = getAlarmPanelId(bedroomControl, zoneId);

		rcu["alarmConfig"] = [
			{
			    "zoneId": "zoneId",
			    "curtainBlindConfig": [
			        {
			            "type": "Blind",
			            "actions": [
			                {
			                    "delay": 0,
			                    "actionType": "OPEN"
			                },
			                {
			                    "delay": 10,
			                    "actionType": "OPEN_PAUSE"
			                }
			            ],
			            "controlIds": [
			                alarmPanelId
			            ]
			        }
			    ]
			}
		];
	}
}

function getAlarmPanelId(bedroomControl, zoneId){

	if(bedroomControl.some(c=>c.id.startsWith(zoneId+"_Blind"))){
		return zoneId+"_BlindPanel";
	}else if(bedroomControl.some(c=>c.id.startsWith(zoneId+"_Curtain"))){
		return zoneId+"_CurtainPanel";
	}else
		throw "[AddAlarm] No _BlindPanel* or _CurtainPanel* found in bedroom controls."; 

}

function removeCurtainSwitch(rcu){
	rcu.command["controls"] = rcu.command.controls.filter(control=>
		control.type!=="Curtain" || control.controlType!=="CurtainSwitch"
	);
}



function testJSON(text) { 
	try { 
		JSON.parse(text); 
		return true; 
	} catch (error) { 
		return false; 
	} 
}

function showResultInTextArea(obj){
	document.getElementById("output").value = prettyPrintJson(obj);
}

function prettyPrintJson(json){
	return JSON.stringify(json, null, 2);
}

function diffUsingDifflib(baseVal, newtxtVal) {

	var base = difflib.stringAsLines(baseVal);
	var newtxt = difflib.stringAsLines(newtxtVal);
	// create a SequenceMatcher instance that diffs the two sets of lines
	var sm = new difflib.SequenceMatcher(base, newtxt);

	// get the opcodes from the SequenceMatcher instance
	// opcodes is a list of 3-tuples describing what changes should be made to the base text
	// in order to yield the new text
	var opcodes = sm.get_opcodes();
	var diffoutputdiv = document.getElementById("diffoutput");
	while (diffoutputdiv.firstChild) diffoutputdiv.removeChild(diffoutputdiv.firstChild);

	var viewType = parseInt(document.querySelector('input[name="view"]:checked').value);

	diffCount = 0;
	// build the diff view and add it to the current DOM
	diffoutputdiv.appendChild(diffview.buildView({
		baseTextLines: base,
		newTextLines: newtxt,
		opcodes: opcodes,
		// set the display titles for each resource
		baseTextName: "Base Text",
		newTextName: "New Text",
		contextSize: null,
		viewType: viewType,
		count: diffCount
	}));
	// scroll down to the diff view window.
	// location = url + "#diff";
}

function addValueIfNotExist(json, key, val){
	if(!json.hasOwnProperty(key) || json[key]===''){
		json[key] = val;
	}
}

function isEmptyArr(arr){
	return !(Array.isArray(arr) && arr.length);
}

function loadSampleJson(){
	var json = prettyPrintJson(parseJson(getSampleJson()));
	document.getElementById("input").innerHTML = json;
}

function updateTimestamp(){
	document.getElementById("timestamp").innerHTML = "Last updated triggered: "+new Date().toUTCString();
}

function isChangeAcCtrlChannel00() {
	return document.getElementById("chkbox_ac_ctrlchannel_to_00").checked;
}

function isChangeTemperatureReadValTo19AE() {
	return document.getElementById("chkbox_temp_readval_to_19AE").checked;
}

function isChangeAcAllowReadTrue() {
	return document.getElementById("chkbox_ac_allowread_true").checked;
}

function isAdd6Delay() {
	return document.getElementById("chkbox_add_6delay").checked;
}

function isAddButler() {
	return document.getElementById("chkbox_add_butler").checked;
}

function isAddLaundry() {
	return document.getElementById("chkbox_add_laundry").checked;
}

function isAddAlarmConfig() {
	return document.getElementById("chkbox_add_alarm").checked;
}

function isRemoveCurtainSwitch() {
	return document.getElementById("chkbox_remove_curtainSwitch").checked;
}


